---
title: "Starting"
date: 2020-04-04T22:53:33-04:00
draft: false
tags: ["Personal","Opinion"]
categories: ["Blog"]
summary: "I guess if this site is going to have a blog I should write something."
---

So I'm overdue to start writing on this blog.  I don't really want the posts to be all pointless jabber, but I have to start somewhere.  I'm hoping to write about personal projects I've worked on at home, focusing on open source software most likely.  I'm also interested in storage technologies so I will probably write about that some as well.  Upcoming topics will include some of the automation work I've done, paperless document management, and load balancing with HAProxy.

If I'm being totally honest part of the point of this post is to test that the auto-deploy scripts still work for the site, so if you're seeing this then they are, hooray!  Anyway, I'll hopefully be more likely to reflect on what I learned on any given project if I write about it, and maybe someone can benefit from what I learned.
