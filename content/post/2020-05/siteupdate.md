---
title: "Quick Update"
date: 2020-05-26T18:07:23-04:00
categories:
- Blog
tags:
- Personal
- Housekeeping
coverSize: partial
autoThumbNailImage: false
coverImage: images/posts/blog/blogging_pexels.jpg
gallery:
    - /images/posts/blog/gallery052020/sanford_1.jpg
    - /images/posts/blog/gallery052020/sanford_2.jpg
    - /images/posts/blog/gallery052020/bay_1.jpg
    - /images/posts/blog/gallery052020/muir_1.jpg
summary: "Got a little direction and a new theme!"
---

It feels great to come back and find Hugo still feels familiar enough to just get started posting again without having to stumble through it - at least not due to Hugo.  It feels even better to find it easier to manipulate themes after cutting my teeth on the previous theme.  This [new theme](https://github.com/kakawait/hugo-tranquilpeak-theme) seems more akin to what I have been thinking of using the site for - sharing technical guides based on projects I've worked on.  It also seems to have some potential for image sharing, which I'm experimenting with on this very post! There's a gallery at the end with some random pictures I had.  There's [a known bug with the theme](https://github.com/kakawait/hugo-tranquilpeak-theme/issues/292) that prevents me from putting captions on them with spaces, so I've left the captions blank. 

That's it for now, just wanted to keep my momentum going after updating the theme and posting my first guide.

<br/>
