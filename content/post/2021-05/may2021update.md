---
title: "Update for May 2021"
date: 2021-05-05
categories:
- Blog
tags:
- Housekeeping
- Blog
coverSize: partial
autoThumbNailImage: false
coverImage: images/posts/blog/blogging_pexels.jpg
summary: "Wait what year is it?"
---

<!--more-->

Happy Cinco de Mayo!  So it's clearly been a while since my last update...  I've not been sitting on my hands but rather they've been too busy to write about what I've been working on.  I hope that going forward I can work out a better balance of writing (reflection) and experimenting with projects (learning).  I think matching up learning and reflection helps it stick - I still remember (almost) everything I wrote here!  At least I remember enough to know I want to make a correction.

### An update on building HAProxy from source

I still highly recommend HAProxy, it's a great application with a surprisingly simple to understand configuration syntax.  The documentation can be a little bit of a beast to get around but I think that is a reflection of how powerful the application really can be.  Anyway, back to the point of this quick update.  I realized a few months back that HAProxy really only uses their GitHub mirror to track current development and the first release of a given minor release, such as 2.2.0 or 2.3.0.  If you want to [stay up to date with development releases](https://github.com/haproxy/haproxy/releases) then this isn't really a problem, but if you want to track stable releases then something else is required.

The HAProxy team maintains a [separate git repository](http://git.haproxy.org/?p=haproxy.git) with all the minor release updates, but it's a little slow to interact with and is probably intended for them to work on and not for constant use in deployment.  For rapid deployment I would instead recommend [grabbing a source tarball](http://www.haproxy.org/download/) to unpack. The build process is the same as I described in [the article I wrote on HAProxy Basics](/2020/05/haproxy-basics-part-1/), and I'm updating that article to reflect this.
