---
title: "Build and install HAProxy from source on Ubuntu 18.04"
date: 2020-06-01T18:16:30-04:00

draft: false
autoThumbnailImage: false
#thumbnailImage: images/posts/guides/javascript_minimess_pexels_thumb.jpg
coverImage: images/posts/guides/javascript_minimess_pexels.jpg
coverSize: partial
tags: ["SysAdmin","HAProxy","Web"]
keywords: ["SysAdmin","HAProxy","Build"]
categories: ["No Fluff Guides", "HAProxy"]
summary: "The \"No Fluff\" guide to install HAProxy from source"
---

### How to build and install HAProxy from source on Ubuntu 18.04

This is the ["No Fluff" guide](https://kevincottrell.net/categories/no-fluff-guides/) for HAProxy installation and basic setup.  For the full guide [start here](https://kevincottrell.net/2020/05/haproxy-basics-part-1/).

Go to the [HAProxy community edition site](https://www.haproxy.org/) and get the download link for the latest release or the release you wish to install.  Make note of it and replace the URL in the wget command and the version in the cd command.  Type the following into a bash shell to install version 2.3.10:

```bash
sudo apt update
sudo apt install build-essential git libssl-dev liblua5.3-dev libsystemd-dev libpcre3-dev zlib1g-dev
wget https://www.haproxy.org/download/2.3/src/haproxy-2.3.10.tar.gz
tar xzf haproxy-2.3.10.tar.gz
cd haproxy-2.3.10
make TARGET=linux-glibc USE_TFO=1 USE_OPENSSL=1 USE_ZLIB=1 USE_LUA=1 USE_PCRE=1 USE_SYSTEMD=1
sudo make install
```

HAProxy is now installed at `/usr/local/bin/haproxy`

### Create your config file

Download [this file](https://github.com/haproxy/haproxy/blob/master/examples/transparent_proxy.cfg) for a transparent proxy and modify the frontend and backend entries to meet your needs.  Keep track of where you save it to use with systemd.

### Run HAProxy as a systemd service

Download this [file](https://github.com/haproxy/haproxy/blob/master/contrib/systemd/haproxy.service.in) and place it in `/etc/systemd/system/haproxy-custom.service` - comment out the lines for the environment file (lines 6 & 7 as of this writing and on tag v2.1.0)and change the part of line 8 that says  `"CONFIG=/etc/haproxy/haproxy.cfg"` to point to your config file.  At this point you can type the following:

```bash
sudo systemctl daemon-reload
sudo systemctl enable haproxy-custom.service
sudo systemctl start haproxy-custom.service
```
