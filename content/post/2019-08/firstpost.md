---
title: "First Post"
date: 2019-08-13T18:20:18-04:00
draft: false 
tags: ["Personal","Opinion","Social Media"] 
categories: ["Blog"]
summary: "This is my first post on the redesigned site."
---

Welcome!  This is my [personal website](https://kevincottrell.net "kevincottrell.net") and primary presence on the internet.  As platforms rise and fall I'd like to hope that the VPS will persist and I can continue to maintain this site far into the future both for posterity and my continued learning.  

My posts will probably be tech focused, if more come at all, but regardless of topic all posts will be my opinion and not necessarily my employers' (assuming I am employed when you read this?) opinions.  I am a big fan of [open source software](https://en.wikipedia.org/wiki/Open-source_software "Wikipedia - Open Source Software") and the number of projects out there that people make available to anyone essentially "for free" depending on the license.  This site (and my career) relies on open source software - if you aren't familiar with it I highly recommend reading about it!

At the moment I have not designed this site to be an interactive discussion.  That is not because I don't care what you have to say, it simply is not part of my current implementation.  With that said I think we could all spend some time learning to listen again.  It's too easy to "turn people off" like they are some kind of TV channel or device.  Think about how many posts you see from folks with things like "not looking for any judgement" or "you have your opinion and I have mine."  We need to be willing to have proper discourse with one another, not just incoherent shouting and "you'll never change my mind" discussion endgames.  We owe it to ourselves to learn from *and* educate the people around us - if we assume we always know better all we are doing is treating each other like children (or worse, _acting_ like children).

Whew sorry for that tangent, once again welcome and thanks for stopping by!  One other thing I plan to do is post tutorials for the various things I do with OSS, so if you are interested in that kind of thing check back or grab the RSS feed from the front page.

-Kevin
