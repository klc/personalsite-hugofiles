---
title: "New section: \"No Fluff\" "
date: 2020-06-02T22:30:51-04:00
categories:
- Blog
tags:
- Housekeeping
- Blog
coverSize: partial
autoThumbNailImage: false
coverImage: images/posts/blog/blogging_pexels.jpg
summary: "For when you ain't got time for all the words."
---

<!--more-->

Recently I made a post in a category called ["No Fluff"](/categories/no-fluff-guides/) and didn't really explain what I was up to. I think it makes sense after reading the contents of the post but I thought a quick blog post could make it clearer - especially while there's only the one guide up on the site.

### Why use many word when few do trick?

I started writing sysadmin/devops guides to both keep track of the things I've learned and to share that information with anyone else who wants to see it.  I can get impatient while I'm searching through sites trying to find a quick solution to a problem, especially when I know I'm just missing one little piece of the puzzle.  We live in a world of blogs that ramble on about how their grandmothers recipe for tomato soup takes them back to their childhood Ratatouille style with the actual recipe you want to see over a thousand words down the page.  

I'd rather not have my site devolve into such a thing if possible, so I had the idea of consolidating my longer posts into one single post that contains only the necessary commands or tidbits required to do exactly what I describe - no extra explanations about what the commands do or where they came from or what I like about them.

If figure this means that someone who blindly follows my No Frills posts will be able to do exactly what I did even if they don't necessarily get the context required to customize it - maybe they don't need to.  In the even they want to they can always find their way to my [longer-winded explanations](/categories/guides/).
