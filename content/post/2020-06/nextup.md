---
title: "Coming next: Jinja Templates and Ansible"
date: 2020-06-13T14:27:37-04:00
categories:
- Blog
tags:
- Housekeeping
keywords:
- Ansible
- Jinja
coverSize: partial
autoThumbNailImage: false
coverImage: images/posts/blog/blogging_pexels.jpg
summary: "Weird name, powerful templating language integrated with Ansible."
---

Hi there!  It's been a busy week because I have a major deadline coming up at work, but I learned some interesting things about [Ansible](https://www.ansible.com/) and its integrated templating language called [Jinja](https://palletsprojects.com/p/jinja/).  Whether you've looked at Ansible or not, I'd argue that when you do whatever task you have in mind will benefit from templating so I highly recommend you look into it.  There's so much to the Ansible documentation and I found most of the [getting started](https://docs.ansible.com/ansible/latest/user_guide/intro_getting_started.html) stuff focused too much on inventories rather than tasks.  I understand the need for this now, but looking back I feel like the way people talk about Ansible it should be easier to make something that "does something" out of the box other than just ping and set NTP.



I have a few posts planned for new guides that I haven't gotten to work on yet, but I hope to have them up within another week or two!  One will probably be related to Jinja as I mentioned in this post, but I've also worked a lot with [Cloud-Init](https://github.com/canonical/cloud-init) recently so I may to try throw that in too.  See you again then!
